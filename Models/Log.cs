﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnimalShop.Models
{
    public class Log
    {
        public Int64 LogID { get; set; }
        public Int64 UserID { get; set; }
        public string TableName { get; set; }
        public string AspSessionID { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

    }
}