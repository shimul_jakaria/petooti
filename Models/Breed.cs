﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AnimalShop.Context;

namespace AnimalShop.Models
{
    public class Breed
    {
        public Int64 BreedID { get; set; }

        public string BreedType { get; set; }

        [Display(Name = "Created Date")]


        public DateTime? CreatedAt { get; set; }
        [Display(Name = "Updated Date")]

        public DateTime? UpdatedAt { get; set; }


        public bool BreedAlReadyExists(AnimalDbContext db, Breed breed)
        {

            var dbbreed = db.Breeds.SingleOrDefault(b => b.BreedType == breed.BreedType);

            if (dbbreed == null)
            {
                return false;
            }
            else
            {
                return true;
            }


        }


    }
}