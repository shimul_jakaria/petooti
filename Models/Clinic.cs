﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnimalShop.Models
{
    public class Clinic
    {
        public Int64 ClinicID { get; set; }
        public string ClinicName { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}