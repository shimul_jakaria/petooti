﻿using AnimalShop.Context;
using AnimalShop.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnimalShop.Models
{
    public class Member
    {
        public Int64 MemberID { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string MobileNumber { get; set; }
        public string Status { get; set; }
        public Int64 LocationID { get; set; }
        public string Password { get; set; }
        public string ConfirmedPassword { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }



        public static MemberViewModel GetMember(long id)
        {
            var db = new AnimalDbContext();
            Member member = db.Members.Find(id);

            MemberViewModel obj = new MemberViewModel
            {
                memberid = member.MemberID,
                fullname = member.FullName,
                Email = member.Email,
                phonenumber = member.MobileNumber,
                location = Convert.ToString(member.LocationID)
            };

            return obj;
        }

    }
}