﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnimalShop.Models
{
    public class Location
    {
        public Int64 LocationID { get; set; }
        public string LocationName { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

    }
}