﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AnimalShop.Models
{

   
    public class Types
    {

        [Key]
        public int TypeId { get; set; }

        public string TypeName { get; set; }

        [Display(Name = "Created Date")]

        public DateTime? CreatedAt { get; set; }
        [Display(Name = "Updated Date")]

        public DateTime? UpdatedAt { get; set; }
    }
}