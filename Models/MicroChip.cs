﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AnimalShop.Models
{
    public class MicroChip
    {
        public Int64 MicroChipID { get; set; }
        [ForeignKey("Clinic")]
        public Int64 ClinicID { get; set; }
        public string MicroChipNumber { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public virtual Clinic Clinic { get; set; }

    }
}