﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AnimalShop.Models
{
    public class Rescue
    {
        [Key]
        public Int64 RescueID { get; set; }
       
        [Display(Name = "Email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
       
      
        public string RescureGroupName { get; set; }
       
        public string ContactPerson { get; set; }
        
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }
        public string FacebookPageUrl { get; set; }
        public string WebsitePageUrl { get; set; }
        public string EmirateLocation { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }
     
      
       
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = "Confirmed Password")]
        [DataType(DataType.Password)]
        public string ConfirmedPassword { get; set; }


        public string AuthKey { get; set; }
        public string SocialID { get; set; }
        public string SocialType { get; set; }
        [Display(Name = "Created Date")]
     
        public DateTime? CreatedAt { get; set; }
        [Display(Name = "Updated Date")]
      
        public DateTime? UpdatedAt { get; set; }


    }
}