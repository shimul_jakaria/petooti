﻿using AnimalShop.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AnimalShop.Context
{
    public class AnimalDbContext : DbContext
    {

        public DbSet<Types> Type { get; set; }

        public DbSet<Rescue> Rescues { get; set; }
        public DbSet<Pet> Pets { get; set; }
        public DbSet<Breed> Breeds { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<AdminPanel> Admins { get; set; }
        public DbSet<Clinic> Clinics { get; set; }
        public DbSet<MicroChip> MicroChips { get; set; }
        public DbSet<Log> Logs { get; set; }

    }
}