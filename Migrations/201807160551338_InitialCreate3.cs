namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Animals", "UserID", "dbo.Users");
            DropIndex("dbo.Animals", new[] { "UserID" });
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationID = c.Long(nullable: false, identity: true),
                        LocationName = c.String(),
                        Longitude = c.Double(),
                        Latitude = c.Double(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.LocationID);
            
            CreateTable(
                "dbo.Members",
                c => new
                    {
                        MemberID = c.Long(nullable: false, identity: true),
                        Role = c.String(),
                        Email = c.String(),
                        FullName = c.String(),
                        UserName = c.String(),
                        MobileNumber = c.String(),
                        Status = c.String(),
                        LocationID = c.Long(nullable: false),
                        Password = c.String(),
                        ConfirmedPassword = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.MemberID);
            
            CreateTable(
                "dbo.Pets",
                c => new
                    {
                        PetID = c.Long(nullable: false, identity: true),
                        UserID = c.Long(nullable: false),
                        AnimalType = c.String(),
                        Image1 = c.String(),
                        Image2 = c.String(),
                        Image3 = c.String(),
                        Breed = c.String(),
                        PetStory = c.String(),
                        Location = c.String(),
                        Gender = c.String(),
                        Age = c.String(),
                        Size = c.String(),
                        CoatLength = c.String(),
                        ActivityLevel = c.String(),
                        TrainingLevel = c.String(),
                        HouseTrained = c.String(),
                        SpecialNeeds = c.String(),
                        Neutered = c.String(),
                        Vaccinated = c.String(),
                        Declawed = c.String(),
                        Compatibility = c.String(),
                        Temperament = c.String(),
                        EnergyLevel = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.PetID);
            
            CreateTable(
                "dbo.Rescues",
                c => new
                    {
                        RescueID = c.Long(nullable: false, identity: true),
                        Email = c.String(),
                        RescureGroupName = c.String(),
                        ContactPerson = c.String(),
                        ContactNumber = c.String(),
                        FacebookPageUrl = c.String(),
                        WebsitePageUrl = c.String(),
                        EmirateLocation = c.String(),
                        Status = c.String(),
                        Password = c.String(),
                        ConfirmedPassword = c.String(),
                        AuthKey = c.String(),
                        SocialID = c.String(),
                        SocialType = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.RescueID);
            
            DropTable("dbo.Animals");
            DropTable("dbo.Users");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        UserRoles = c.String(),
                        Email = c.String(),
                        UserName = c.String(nullable: false),
                        FullName = c.String(),
                        MobileNumber = c.String(),
                        Status = c.String(),
                        Image = c.String(),
                        Address = c.String(),
                        Password = c.String(),
                        ConfirmedPassword = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Animals",
                c => new
                    {
                        AnimalId = c.Long(nullable: false, identity: true),
                        UserID = c.Long(nullable: false),
                        AnimalType = c.String(),
                        Image1 = c.String(),
                        Image2 = c.String(),
                        Image3 = c.String(),
                        Breed = c.String(),
                        PetStory = c.String(),
                        Location = c.String(),
                        Gender = c.String(),
                        Age = c.String(),
                        Size = c.String(),
                        CoatLength = c.String(),
                        ActivityLevel = c.String(),
                        TrainingLevel = c.String(),
                        HouseTrained = c.String(),
                        SpecialNeeds = c.String(),
                        Neutered = c.String(),
                        Vaccinated = c.String(),
                        Declawed = c.String(),
                        Compatibility = c.String(),
                        Temperament = c.String(),
                        EnergyLevel = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.AnimalId);
            
            DropTable("dbo.Rescues");
            DropTable("dbo.Pets");
            DropTable("dbo.Members");
            DropTable("dbo.Locations");
            CreateIndex("dbo.Animals", "UserID");
            AddForeignKey("dbo.Animals", "UserID", "dbo.Users", "UserId", cascadeDelete: true);
        }
    }
}
