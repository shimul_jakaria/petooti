namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animals",
                c => new
                    {
                        AnimalId = c.Long(nullable: false, identity: true),
                        UserID = c.Long(nullable: false),
                        AnimalType = c.String(),
                        Image1 = c.String(),
                        Image2 = c.String(),
                        Image3 = c.String(),
                        Breed = c.String(),
                        PetStory = c.String(),
                        Location = c.String(),
                        Gender = c.String(),
                        Age = c.String(),
                        Size = c.String(),
                        CoatLength = c.String(),
                        ActivityLevel = c.String(),
                        TrainingLevel = c.String(),
                        HouseTrained = c.String(),
                        SpecialNeeds = c.String(),
                        Neutered = c.String(),
                        Vaccinated = c.String(),
                        Declawed = c.String(),
                        Compatibility = c.String(),
                        Temperament = c.String(),
                        EnergyLevel = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.AnimalId)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Animals", "UserID", "dbo.Users");
            DropIndex("dbo.Animals", new[] { "UserID" });
            DropTable("dbo.Animals");
        }
    }
}
