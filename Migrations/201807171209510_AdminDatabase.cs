namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdminDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        AdminID = c.Long(nullable: false, identity: true),
                        Role = c.String(),
                        Email = c.String(),
                        UserName = c.String(),
                        FullName = c.String(),
                        MobileNumber = c.String(),
                        Address = c.String(),
                        Password = c.String(),
                        ConfirmedPassword = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.AdminID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Admins");
        }
    }
}
