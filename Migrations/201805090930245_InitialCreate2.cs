namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Breeds",
                c => new
                    {
                        BreedID = c.Long(nullable: false, identity: true),
                        BreedType = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.BreedID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Breeds");
        }
    }
}
