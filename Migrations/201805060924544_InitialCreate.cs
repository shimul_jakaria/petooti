namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        UserRoles = c.String(),
                        Email = c.String(),
                        UserName = c.String(nullable: false),
                        FullName = c.String(),
                        MobileNumber = c.String(),
                        Status = c.String(),
                        Image = c.String(),
                        Address = c.String(),
                        Password = c.String(),
                        ConfirmedPassword = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
        }
    }
}
