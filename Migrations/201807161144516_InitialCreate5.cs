namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pets", "PetName", c => c.String());
            AddColumn("dbo.Pets", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pets", "Description");
            DropColumn("dbo.Pets", "PetName");
        }
    }
}
