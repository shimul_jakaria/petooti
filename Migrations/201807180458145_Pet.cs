namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Pet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pets", "PetStory", c => c.String());
            AddColumn("dbo.Pets", "PetCharacteristics", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pets", "PetCharacteristics");
            DropColumn("dbo.Pets", "PetStory");
        }
    }
}
