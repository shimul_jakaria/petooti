namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Clinic : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clinics",
                c => new
                    {
                        ClinicID = c.Long(nullable: false, identity: true),
                        ClinicName = c.String(),
                        Address = c.String(),
                        ContactNumber = c.String(),
                        Email = c.String(),
                        Longitude = c.Double(),
                        Latitude = c.Double(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.ClinicID);
            
            CreateTable(
                "dbo.MicroChips",
                c => new
                    {
                        MicroChipID = c.Long(nullable: false, identity: true),
                        ClinicID = c.Long(nullable: false),
                        MicroChipNumber = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.MicroChipID)
                .ForeignKey("dbo.Clinics", t => t.ClinicID, cascadeDelete: true)
                .Index(t => t.ClinicID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MicroChips", "ClinicID", "dbo.Clinics");
            DropIndex("dbo.MicroChips", new[] { "ClinicID" });
            DropTable("dbo.MicroChips");
            DropTable("dbo.Clinics");
        }
    }
}
