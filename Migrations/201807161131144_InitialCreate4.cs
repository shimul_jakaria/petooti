namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pets", "RescueID", c => c.Long(nullable: false));
            AddColumn("dbo.Pets", "TypeID", c => c.Long(nullable: false));
            AddColumn("dbo.Pets", "BreedID", c => c.Long(nullable: false));
            AddColumn("dbo.Pets", "LocationID", c => c.Long(nullable: false));
            DropColumn("dbo.Pets", "UserID");
            DropColumn("dbo.Pets", "AnimalType");
            DropColumn("dbo.Pets", "Breed");
            DropColumn("dbo.Pets", "PetStory");
            DropColumn("dbo.Pets", "Location");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pets", "Location", c => c.String());
            AddColumn("dbo.Pets", "PetStory", c => c.String());
            AddColumn("dbo.Pets", "Breed", c => c.String());
            AddColumn("dbo.Pets", "AnimalType", c => c.String());
            AddColumn("dbo.Pets", "UserID", c => c.Long(nullable: false));
            DropColumn("dbo.Pets", "LocationID");
            DropColumn("dbo.Pets", "BreedID");
            DropColumn("dbo.Pets", "TypeID");
            DropColumn("dbo.Pets", "RescueID");
        }
    }
}
