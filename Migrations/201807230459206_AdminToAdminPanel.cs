namespace AnimalShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdminToAdminPanel : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Admins", newName: "AdminPanels");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.AdminPanels", newName: "Admins");
        }
    }
}
