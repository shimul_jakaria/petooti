﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AnimalShop.Context;




namespace AnimalShop.Areas.Admin.Controllers
{
    public class RescueController : Controller
    {
        private AnimalDbContext db;




        public RescueController()
        {
            db = new AnimalDbContext();

        }


        // GET: Admin/Rescue
        public ActionResult Index()
        {
            return View(db.Rescues
                          .OrderByDescending(s => s.RescueID)
                          .ToList());
        }

        // 


        public JsonResult AddRescue(Models.Rescue rescue)
        {
            if (ModelState.IsValid)
            {

                rescue.UpdatedAt = DateTime.Now;
                rescue.CreatedAt = DateTime.Now;

                db.Rescues.Add(rescue);
                db.SaveChanges();

                return Json("Successfully!!!!", JsonRequestBehavior.AllowGet);
            }


            return Json("Failed", JsonRequestBehavior.AllowGet);
        }



        // GET: Admin/Rescue/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            Models.Rescue rescue = db.Rescues.Find(id);
            if (rescue == null)
            {
                return HttpNotFound();
            }
            return View(rescue);
        }

        // GET: Admin/Rescue/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Rescue/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.Rescue rescue)
        {
            if (ModelState.IsValid)
            {

                rescue.UpdatedAt = DateTime.Now;
                rescue.CreatedAt = DateTime.Now;

                db.Rescues.Add(rescue);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rescue);
        }

        // GET: Admin/Rescue/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Rescue rescue = db.Rescues.Find(id);
            if (rescue == null)
            {
                return HttpNotFound();
            }
            return View(rescue);
        }

        // POST: Admin/Rescue/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RescueID,Email,RescureGroupName,ContactPerson,ContactNumber,FacebookPageUrl,WebsitePageUrl,EmirateLocation,Status,Password,ConfirmedPassword,AuthKey,SocialID,SocialType,CreatedAt,UpdatedAt")] Models.Rescue rescue)
        {
            if (ModelState.IsValid)
            {
                var dbrescue = db.Rescues.SingleOrDefault(r => r.RescueID == rescue.RescueID);


                if (dbrescue != null)
                {
                    rescue.CreatedAt = dbrescue.CreatedAt;
                }


                rescue.UpdatedAt = DateTime.Now;


                db.Entry(rescue).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rescue);
        }

        // GET: Admin/Rescue/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Rescue rescue = db.Rescues.Find(id);
            if (rescue == null)
            {
                return HttpNotFound();
            }
            return View(rescue);
        }

        // POST: Admin/Rescue/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Models.Rescue rescue = db.Rescues.Find(id);
            db.Rescues.Remove(rescue);
            db.SaveChanges();
            return RedirectToAction("Index");
        }





        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
