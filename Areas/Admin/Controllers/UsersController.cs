﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AnimalShop.Context;
using AnimalShop.Models;
using handyman.DataAccess.Admin;

namespace AnimalShop.Areas.Admin.Controllers
{
    public class UsersController : Controller
    {
        private AnimalDbContext db = new AnimalDbContext();

        // GET: Admin/Users
        public ActionResult Index()
        {
            //return View(db.Users.ToList());
            return View();
        }

        // GET: Admin/Users/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member user = db.Members.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Admin/Users/Create
        public ActionResult Create()
        {
            return View();
        }



        public JsonResult UserList()
        {
            return Json(db.Members.ToList(), JsonRequestBehavior.AllowGet);
        }

        // POST: Admin/Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        public JsonResult UsersCreate(string Fullname, string Userroles, string Mobilenumber, string Email, string Username, string Password, string Status, string Address)
        {



            var member = new Member
            {
                //FullName = Fullname,
                //UserRoles = Userroles,

                //Email = Email,
                //UserName = Username,
                //MobileNumber = Mobilenumber,
                //Status = Status,
                //Address = Address,
                //Password = PasswordHash.Hash(Password)


            };

            var search_existdata = (from n in db.Members
                                    where n.UserName == Username && n.Email == Email
                                    select n).ToList().Count();



            if (search_existdata == 0)
            {

                member.CreatedAt = DateTime.Now;
                member.UpdatedAt = DateTime.Now;

                db.Members.Add(member);
                db.SaveChanges();
                TempData["clientMessage"] = "Created Successfully";
            }
            else
            {
                TempData["clientMessage"] = "Same Name and Email exists";
            }

            var clientmsg = TempData["clientMessage"];





            return Json(clientmsg.ToString(), JsonRequestBehavior.AllowGet);
        }

        // GET: Admin/Users/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member user = db.Members.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Admin/Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "UserId,UserRoles,Email,UserName,FullName,MobileNumber,Status,Image,Address,Password,ConfirmedPassword,CreatedAt,UpdatedAt")] User user)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(user).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(user);
        //}

        // GET: Admin/Users/Delete/5
        //public ActionResult Delete(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    User user = db.Users.Find(id);
        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(user);
        //}

        // POST: Admin/Users/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(long id)
        //{
        //    User user = db.Users.Find(id);
        //    db.Users.Remove(user);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
