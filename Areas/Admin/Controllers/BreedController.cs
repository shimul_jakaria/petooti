﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AnimalShop.Context;
using AnimalShop.Models;

namespace AnimalShop.Areas.Admin.Controllers
{
    public class BreedController : Controller
    {
        private AnimalDbContext db = new AnimalDbContext();

        // GET: Admin/Breed
        public async Task<ActionResult> Index()
        {
            return View(await db.Breeds.ToListAsync());
        }

        // GET: Admin/Breed/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Breed breed = await db.Breeds.FindAsync(id);
            if (breed == null)
            {
                return HttpNotFound();
            }
            return View(breed);
        }

        // GET: Admin/Breed/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Breed/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BreedID,BreedType,CreatedAt,UpdatedAt")] Breed breed)
        {
            if (ModelState.IsValid)
            {



                if (new Breed().BreedAlReadyExists(db, breed))

                {
                    ViewBag.MSG = "Breed Already Exists!";
                    return View("Create", breed);
                }
                else if (String.IsNullOrEmpty(breed.BreedType))
                {
                    ViewBag.MSG = "Please  Provide Breed Type";
                    return View("Create", breed);
                }
                else
                {


                    breed.CreatedAt = DateTime.Now;
                    breed.UpdatedAt = DateTime.Now;

                    db.Breeds.Add(breed);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }




            }

            return View(breed);
        }

        // GET: Admin/Breed/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Breed breed = await db.Breeds.FindAsync(id);
            if (breed == null)
            {
                return HttpNotFound();
            }
            return View(breed);
        }

        // POST: Admin/Breed/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "BreedID,BreedType,CreatedAt,UpdatedAt")] Breed breed)
        {
            if (ModelState.IsValid)
            {
                var dbBreed = db.Breeds.SingleOrDefault(m => m.BreedID == breed.BreedID);

                dbBreed.BreedType = breed.BreedType;
                dbBreed.UpdatedAt = DateTime.Now;

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(breed);
        }

        // GET: Admin/Breed/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Breed breed = await db.Breeds.FindAsync(id);
            if (breed == null)
            {
                return HttpNotFound();
            }
            return View(breed);
        }

        // POST: Admin/Breed/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Breed breed = await db.Breeds.FindAsync(id);
            db.Breeds.Remove(breed);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
