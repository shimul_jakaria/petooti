﻿using AnimalShop.Context;
using AnimalShop.Models;
using handyman.DataAccess.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AnimalShop.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        AnimalDbContext db = new AnimalDbContext();
        // GET: Admin/Login
        public ActionResult Index()
        {
           
           
         
            return View();
        }


        [HttpPost]
        public ActionResult Index(AdminPanel model)
        {


           // string pass = PasswordHash.Hash(model.Password);
            var search_role = (from n in db.Admins where n.Email == model.Email && n.Password == model.Password select n).ToList();
           
            foreach (var role in search_role)
            {

                if (role.Role == "Admin")
                {

                    Session["AdminId"] = role.AdminID;
                    Session["Roles"] = role.Role;
                    Session["AdminName"] = role.UserName;
                    string sessionID = Session.SessionID;
                    return RedirectToAction("Index","DashBoard");


                }
                else
                {
                    //Session["UserId"] = role.UserId.ToString();
                    //Session["UserRoles"] = role.UserRoles;
                    //Session["UserName"] = role.UserName;
                    //return RedirectToAction("Index", "AnimalHouse", new { area = "Rescue" });
                }
            }
            

            return RedirectToAction("Index");
        }
    }
}