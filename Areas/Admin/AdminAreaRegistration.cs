﻿using System.Web.Mvc;

namespace AnimalShop.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            context.MapRoute(
               "Admin_login",
               "",
               new { controller = "Login", action = "Index", id = UrlParameter.Optional }
           );

            context.MapRoute(
           "Admin_dashboard",
           "admin/dashboard",
           new { controller = "DashBoard", action = "Index", id = UrlParameter.Optional }
           );






            context.MapRoute(
        "Admin_Users",
        "admin/users",
        new { controller = "Users", action = "Index", id = UrlParameter.Optional }
        );


            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}