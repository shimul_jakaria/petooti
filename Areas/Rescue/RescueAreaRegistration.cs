﻿using System.Web.Mvc;

namespace AnimalShop.Areas.Rescue
{
    public class RescueAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Rescue";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {

            context.MapRoute(
          "Rescue_AnimalHouse",
          "rescue/addanimal",
          new { controller = "AnimalHouse", action = "Index", id = UrlParameter.Optional }
          );



            context.MapRoute(
                "Rescue_default",
                "Rescue/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}