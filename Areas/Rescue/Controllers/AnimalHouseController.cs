﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using AnimalShop.Context;
using AnimalShop.Models;
using AnimalShop.Utility;

namespace AnimalShop.Areas.Rescue.Controllers
{
    public class AnimalHouseController : Controller
    {


        private readonly AnimalDbContext _context;

        public AnimalHouseController()
        {
            _context = new AnimalDbContext();
        }

        // GET: Rescue/AnimalHouse
        public ActionResult Index()
        {
            return View();
        }



        [HttpPost]
        public ActionResult SaveAnimals(Pet animal, IEnumerable<HttpPostedFileBase> file)
        {
            if (ModelState.IsValid)
            {


                string path = Server.MapPath("~/Uploads/");
                var listOfFIleName = new MultipalFileUpload().UploadMultipalFile(file, path);





            }




            return View();
        }





        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }



    }

}