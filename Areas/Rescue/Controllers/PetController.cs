﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AnimalShop.Context;
using AnimalShop.ViewModel;

namespace AnimalShop.Areas.Rescue.Controllers
{
    public class PetController : Controller
    {
        private AnimalDbContext db;


        public PetController()
        {
            db = new AnimalDbContext();
        }


        // GET: Pet
        public ActionResult Index()
        {

            string mainUrl = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + "/";


            var pets = db.Pets.OrderByDescending(p => p.PetID)
                              .Select(p => new PetViewModel()
                              {
                                  PetID = p.PetID,
                                  PetName = p.PetName,
                                  Image1 = mainUrl+ p.Image1,
                                  Image2 = mainUrl+ p.Image2,
                                  Image3 =mainUrl + p.Image3,
                                  Age = p.Age,
                                  Gender = p.Gender,
                                  Compatibility = p.Compatibility,
                                  EnergyLevel = p.EnergyLevel,
                                  Temperament = p.Temperament,
                                  BreedName = db.Breeds.FirstOrDefault(b => b.BreedID == p.BreedID).BreedType,
                                  TypeName = db.Type.FirstOrDefault(b => b.TypeId == p.TypeID).TypeName,
                                  CoatLength = p.CoatLength,
                                  Declawed = p.Declawed,
                                  Vaccinated = p.Vaccinated,
                                  TrainingLevel = p.TrainingLevel,
                                  SpecialNeeds = p.SpecialNeeds,
                                  Size = p.Size,
                                  PetStory = p.PetStory,
                                  ActivityLevel = p.ActivityLevel,
                                  Description = p.Description,
                                  HouseTrained = p.HouseTrained,
                                  RescueName = db.Rescues.FirstOrDefault(r => r.RescueID == p.RescueID).RescureGroupName
                              })
                              .ToList();


            return View(pets);
        }





        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}