﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace AnimalShop.Utility
{
    public class MultipalFileUpload
    {

        public IEnumerable<String> UploadMultipalFile(IEnumerable<HttpPostedFileBase> file, string path)
        {

            //bool ok = false;

            List<String> fileLinkList = new List<string>();

            foreach (var postedFile in file)
            {
                if (postedFile != null)
                {
                    String fileLink = String.Empty;

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    string filename = postedFile.FileName
                                      + DateTime.Now.DayOfYear
                                      + DateTime.Now.DayOfWeek
                                      + DateTime.Now.Month
                                      + DateTime.Now.Hour
                                      + DateTime.Now.Minute
                                      + DateTime.Now.Second
                                      + DateTime.Now.Millisecond;


                    filename = Path.Combine(path, filename);
                    postedFile.SaveAs(filename);
                    fileLink += "." + postedFile.ContentType;
                    fileLinkList.Add(fileLink);
                }

            }
            return fileLinkList;
        }


    }
}