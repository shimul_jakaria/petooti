﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnimalShop.ViewModel
{
    public class PetViewModel
    {
        public Int64 PetID { get; set; }
        public string PetName { get; set; }
        public Int64 RescueID { get; set; }
        public string RescueName { get; set; }
        public string Description { get; set; }
        public Int64 TypeID { get; set; }
        public string TypeName { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public Int64 BreedID { get; set; }
        public string BreedName { get; set; }

        public Int64 LocationID { get; set; }
        public string LocationName { get; set; }

        public string Gender { get; set; }
        public string Age { get; set; }
        public string Size { get; set; }
        public string CoatLength { get; set; }
        public string ActivityLevel { get; set; }
        public string TrainingLevel { get; set; }
        public string HouseTrained { get; set; }
        public string SpecialNeeds { get; set; }
        public string Neutered { get; set; }
        public string Vaccinated { get; set; }
        public string Declawed { get; set; }
        public string Compatibility { get; set; }
        public string Temperament { get; set; }
        public string EnergyLevel { get; set; }
        public string PetStory { get; set; }
        public string PetCharacteristics { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }


        public PetViewModel()
        {

        }





    }
}