﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AnimalShop.ViewModel
{
    public class MemberViewModel
    {
        public Int64 memberid { get; set; }
        
        public string fullname { get; set; }
      
      
        public string Email { get; set; }
       
        public string phonenumber { get; set; }
       
      
        public string location { get; set; }


    

        public DateTime? CreatedAt { get; set; }
        [Display(Name = "Updated Date")]

        public DateTime? UpdatedAt { get; set; }

    }
}