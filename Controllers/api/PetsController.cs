﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AnimalShop.Context;
using AnimalShop.Models;
using System.Web;

namespace AnimalShop.Controllers
{
    public class PetsController : ApiController
    {
        private AnimalDbContext db = new AnimalDbContext();

        // GET: api/Pets
        public IHttpActionResult GetPets()
        {

            //HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)
            var petlist = new List<object>();
            var pets = db.Pets.ToList();
            foreach (var ss in pets)
            {
                var search_locationName = (from n in db.Locations where n.LocationID == ss.LocationID select n).FirstOrDefault();
                var search_breedName = (from n in db.Breeds where n.BreedID == ss.BreedID select n).FirstOrDefault();
                petlist.Add(new
                {
                    PetID = ss.PetID,
                    PetName = ss.PetName,
                    Image1 = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + ss.Image1,
                    LocationID = ss.LocationID,
                    LocationName = search_locationName.LocationName,
                    Gender = ss.Gender,
                    Age = ss.Age,
                    BreedName = search_breedName.BreedType,
                    BreedID = ss.BreedID,
                    Description = ss.Description

                });
            }


            return Content(HttpStatusCode.OK, new { data = new { petlist = petlist }, code = HttpStatusCode.OK, message = "success", isSuccess = true });
        }


        public IHttpActionResult GetPetSearchParams()
        {

            var types = db.Type.Select(t =>
                                        new
                                        {
                                            id = t.TypeId,
                                            name = t.TypeName
                                        });



            var breeds = db.Breeds.Select(t =>
                  new
                  {
                      id = t.BreedID,
                      name = t.BreedType
                  });


            var genders = db.Pets.Select(p => p.Gender)
                                 .Distinct()
                                 .ToList();






            var ages = db.Pets.Select(p => p.Age)
                              .Distinct()
                              .ToList();

            var compatibilities = db.Pets.Select(p => p.Compatibility)
                                         .Distinct()
                                         .ToList();


            var temperaments = db.Pets.Select(p => p.Temperament)
                                                     .Distinct()
                                                     .ToList();



            var energyLevels = db.Pets.Select(p => p.EnergyLevel)
                                      .Distinct()
                                      .ToList();



            var locations = db.Locations.Select(t =>
                new
                {
                    id = t.LocationID,
                    name = t.LocationName
                });



            return Content(HttpStatusCode.OK, new
            {
                data = new
                {
                    types,
                    breeds,
                    genders,
                    ages,
                    compatibilities,
                    temperaments,
                    energyLevels,
                    locations


                },
                code = HttpStatusCode.OK,
                message = "success",
                isSuccess = true
            });

        }






        // GET: api/Pets/5
        [ResponseType(typeof(Pet))]
        public IHttpActionResult GetPet(long id)
        {

            var petlist = new List<object>();
            List<string> imagelist = new List<string>();
            var pets = db.Pets.Where(x => x.PetID == id).FirstOrDefault();


            var search_locationName = (from n in db.Locations where n.LocationID == pets.LocationID select n).FirstOrDefault();
            var search_breedName = (from n in db.Breeds where n.BreedID == pets.BreedID select n).FirstOrDefault();
            imagelist.Add(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + pets.Image1);
            imagelist.Add(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + pets.Image2);
            imagelist.Add(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + pets.Image3);
            petlist.Add(new
            {
                PetID = pets.PetID,
                PetName = pets.PetName,
                //Image1 = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + pets.Image1,
                //Imgae2 = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + pets.Image2,
                //Imgae3 = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + pets.Image3,
                ImageList = imagelist,
                LocationID = pets.LocationID,
                LocationName = search_locationName.LocationName,
                BreedName = search_breedName.BreedType,
                BreedID = pets.BreedID,
                PetStory = pets.PetStory,
                PetCharacteristics = pets.PetCharacteristics,
                Gender = pets.Gender,
                Age = pets.Age
            });


            return Content(HttpStatusCode.OK, new { data = new { petlist = petlist }, code = HttpStatusCode.OK, message = "success", isSuccess = true });
        }

        // PUT: api/Pets/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPet(long id, Pet pet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pet.PetID)
            {
                return BadRequest();
            }

            db.Entry(pet).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pets
        [ResponseType(typeof(Pet))]
        public IHttpActionResult PostPet(Pet pet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Pets.Add(pet);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pet.PetID }, pet);
        }

        // DELETE: api/Pets/5
        [ResponseType(typeof(Pet))]
        public IHttpActionResult DeletePet(long id)
        {
            Pet pet = db.Pets.Find(id);
            if (pet == null)
            {
                return NotFound();
            }

            db.Pets.Remove(pet);
            db.SaveChanges();

            return Ok(pet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PetExists(long id)
        {
            return db.Pets.Count(e => e.PetID == id) > 0;
        }
    }
}