﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AnimalShop.Context;
using AnimalShop.Models;
using AnimalShop.ViewModel;

namespace AnimalShop.Controllers
{
    public class MembersController : ApiController
    {
        private AnimalDbContext db = new AnimalDbContext();

        // GET: api/Members
        public IHttpActionResult GetMembers()
        {
            return Content(HttpStatusCode.OK, new { data =db.Members, code = HttpStatusCode.OK, message = "success", isSuccess = true });
        }

        // GET: api/Members/5
      
      

        // PUT: api/Members/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMember(long id, Member member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != member.MemberID)
            {
                return BadRequest();
            }

            db.Entry(member).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MemberExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Members
        [ResponseType(typeof(Member))]
        public IHttpActionResult PostMember(Member member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(member==null)
            {
                return Content(HttpStatusCode.OK, new
                {
                    data = new
                    {
                        member = new
                        {
                        }
                    },
                    code = HttpStatusCode.NotFound,
                    message = "Please fill up the form!",
                    isSuccess = false
                });
            }

            db.Members.Add(member);
            db.SaveChanges();




            return Content(HttpStatusCode.OK, new { data =new { member= Member.GetMember(member.MemberID) }, code = HttpStatusCode.OK, message = "success", isSuccess = true });
        }

        // DELETE: api/Members/5
        [ResponseType(typeof(Member))]
        public IHttpActionResult DeleteMember(long id)
        {
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return NotFound();
            }

            db.Members.Remove(member);
            db.SaveChanges();

            return Ok(member);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MemberExists(long id)
        {
            return db.Members.Count(e => e.MemberID == id) > 0;
        }
    }
}