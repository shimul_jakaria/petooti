﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AnimalShop.Context;
using AnimalShop.Models;

namespace AnimalShop.Controllers
{
    public class ClinicsController : ApiController
    {
        private AnimalDbContext db = new AnimalDbContext();

        // GET: api/Clinics
        public IQueryable<Clinic> GetClinics()
        {
            return db.Clinics;
        }

        // GET: api/Clinics/5
        [ResponseType(typeof(Clinic))]
        public IHttpActionResult GetClinic(long id)
        {
            Clinic clinic = db.Clinics.Find(id);
            if (clinic == null)
            {
                return NotFound();
            }

            return Ok(clinic);
        }

        // PUT: api/Clinics/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutClinic(long id, Clinic clinic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != clinic.ClinicID)
            {
                return BadRequest();
            }

            db.Entry(clinic).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClinicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Clinics
        [ResponseType(typeof(Clinic))]
        public IHttpActionResult PostClinic(Clinic clinic)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Clinics.Add(clinic);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = clinic.ClinicID }, clinic);
        }

        // DELETE: api/Clinics/5
        [ResponseType(typeof(Clinic))]
        public IHttpActionResult DeleteClinic(long id)
        {
            Clinic clinic = db.Clinics.Find(id);
            if (clinic == null)
            {
                return NotFound();
            }

            db.Clinics.Remove(clinic);
            db.SaveChanges();

            return Ok(clinic);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClinicExists(long id)
        {
            return db.Clinics.Count(e => e.ClinicID == id) > 0;
        }
    }
}