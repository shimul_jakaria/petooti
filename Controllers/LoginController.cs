﻿using AnimalShop.Context;
using AnimalShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AnimalShop.Controllers
{
    public class LoginController : ApiController
    {
        private AnimalDbContext db = new AnimalDbContext();
        public IHttpActionResult GetMemberLogin(string email,string password)
        {
           
            if (email=="" && password=="")
            {
                return Content(HttpStatusCode.OK, new
                {
                    data = new
                    {
                        member = new
                        {
                        }
                    },
                    code = HttpStatusCode.NotFound,
                    message = "Please fill up the form!",
                    isSuccess = false
                });
            }
            var check_login = (from n in db.Members where n.Email == email && n.Password == password select n).FirstOrDefault();

            return Content(HttpStatusCode.OK, new { data = new { member = Member.GetMember(check_login.MemberID) }, code = HttpStatusCode.OK, message = "success", isSuccess = true });
        }


    }
}
