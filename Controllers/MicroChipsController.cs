﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AnimalShop.Context;
using AnimalShop.Models;

namespace AnimalShop.Controllers
{
    public class MicroChipsController : ApiController
    {
        private AnimalDbContext db = new AnimalDbContext();

        // GET: api/MicroChips
        public IQueryable<MicroChip> GetMicroChips()
        {
            return db.MicroChips;
        }

        // GET: api/MicroChips/5
        [ResponseType(typeof(MicroChip))]
        public IHttpActionResult GetMicroChip(string microchipNumber)
        {
            var clinicId_find = db.MicroChips.Where(x => x.MicroChipNumber == microchipNumber).FirstOrDefault();
            var getclinic = new List<object>();
            
            var clinicData = db.Clinics.Where(x => x.ClinicID == clinicId_find.ClinicID).FirstOrDefault();


            //var search_locationName = (from n in db.Locations where n.LocationID == pets.LocationID select n).FirstOrDefault();
            //var search_breedName = (from n in db.Breeds where n.BreedID == pets.BreedID select n).FirstOrDefault();

            getclinic.Add(new
            {
                ClinicID = clinicData.ClinicID,
                ClinicName = clinicData.ClinicName,
                Address=clinicData.Address,
                ContactNumber=clinicData.ContactNumber,
                Email=clinicData.Email,
                Longitude=clinicData.Longitude,
                Latitude=clinicData.Latitude



            });


            return Content(HttpStatusCode.OK, new { data = new { clinic = getclinic }, code = HttpStatusCode.OK, message = "success", isSuccess = true });
        }

        // PUT: api/MicroChips/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMicroChip(long id, MicroChip microChip)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != microChip.MicroChipID)
            {
                return BadRequest();
            }

            db.Entry(microChip).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MicroChipExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MicroChips
        [ResponseType(typeof(MicroChip))]
        public IHttpActionResult PostMicroChip(MicroChip microChip)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MicroChips.Add(microChip);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = microChip.MicroChipID }, microChip);
        }

        // DELETE: api/MicroChips/5
        [ResponseType(typeof(MicroChip))]
        public IHttpActionResult DeleteMicroChip(long id)
        {
            MicroChip microChip = db.MicroChips.Find(id);
            if (microChip == null)
            {
                return NotFound();
            }

            db.MicroChips.Remove(microChip);
            db.SaveChanges();

            return Ok(microChip);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MicroChipExists(long id)
        {
            return db.MicroChips.Count(e => e.MicroChipID == id) > 0;
        }
    }
}