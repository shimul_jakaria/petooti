﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AnimalShop
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();



            //GetPetSearchParams

            // Mahfuz

            config.Routes.MapHttpRoute(
                "GetPetSearchParams",
                "api/search_params",
                new { controller = "Pets", action = "GetPetSearchParams", id = RouteParameter.Optional }
            );
            // Mahfuz




            config.Routes.MapHttpRoute(
               "GetMemberList",
               "api/members",
                new { controller = "Members", action = "GetMembers", id = RouteParameter.Optional }
           );

            config.Routes.MapHttpRoute(
              "GetLocationList",
              "api/locations",
               new { controller = "Locations", action = "GetLocations", id = RouteParameter.Optional }
          );

            config.Routes.MapHttpRoute(
           "MemberSignUp",
           "api/signup",
            new { controller = "Members", action = "PostMember", id = RouteParameter.Optional }
       );
            config.Routes.MapHttpRoute(
      "MemberSignIn",
      "api/login",
       new { controller = "Login", action = "GetMemberLogin", id = RouteParameter.Optional }
  );
            config.Routes.MapHttpRoute(
           "PetList",
           "api/petlist",
            new { controller = "Pets", action = "GetPets", id = RouteParameter.Optional }
       );

            config.Routes.MapHttpRoute(
           "PetDetails",
           "api/pet/{id}",
            new { controller = "Pets", action = "GetPet", id = RouteParameter.Optional }
       );

            config.Routes.MapHttpRoute(
        "ClinicByMicrochip",
        "api/clinic/microchip/{microchipNumber}",
         new { controller = "MicroChips", action = "GetMicroChip", microchipNumber = RouteParameter.Optional }
    );



            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
